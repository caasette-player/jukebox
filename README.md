# Jukebox

## Informatics
The server code that serves up the component requested when given a lookup string

### Lookup String
* path: `'@caasette/component/export'`
* path: `'@caasette/component'`

ONLY care about `'@caasette/component'` tho and then return. Anything can be added after the `component/` and still work. There will be no validation there as it isn't important nor does it make sense to. Maybe for security reasons?

### Error Caases
* If component doesn't exist then either
    * return a `UMD` module that contains an error component
    * throw a `404` error.

### Happy Path Caases
* Return a JavaScript string.
```
script: http://script
```

## TODO
* Uses `hapi`
* Currently uses static files, this needs to change
* Is it's own repo (here)
